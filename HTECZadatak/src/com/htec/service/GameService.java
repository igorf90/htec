package com.htec.service;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.htec.bl.GameBL;
import com.htec.request.ChangeResultRequest;
import com.htec.request.GameRequest;

@Path(value = "/gameService")
public class GameService {

	
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path(value="returnGames")
	public ArrayList returnGames(GameRequest gameRequest) throws Exception{
		GameBL gameBL = new GameBL();
		return gameBL.returnMatchResults(gameRequest);
	}
	
	
	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	@Path(value="updateResult")
	public void updateResult(@WebParam(name = "") List<ChangeResultRequest> changeResultRequests) throws Exception{
		GameBL gameBL = new GameBL();
		gameBL.updateResult((ArrayList<ChangeResultRequest>) changeResultRequests);
	}
	
}
