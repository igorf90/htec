package com.htec.service;



import java.util.ArrayList;
import java.util.List;

import javax.jws.WebParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.htec.bl.GameBL;
import com.htec.bl.TableBL;
import com.htec.model.Game;
import com.htec.model.Table;

@Path(value = "/tableService")
public class TableService {
	
	
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@Path(value="returnTable")
	public Table returnTable(@WebParam(name = "") List<Game> gameRequest) throws Exception{
		TableBL tableBL = new TableBL();
		GameBL gameBL = new GameBL();
		gameBL.saveGame((ArrayList) gameRequest);
		return tableBL.getTable((ArrayList<Game>) gameRequest);
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.TEXT_PLAIN})
	@Path(value="returnSavedTable/{group}")
	public ArrayList returnSavedTable(@PathParam(value = "group") String group) throws Exception{
		TableBL tableBL = new TableBL();
		return tableBL.returnSavedTable(group);
	}
	
	

}
