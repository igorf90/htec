package com.htec.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static String dateToString (Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateS = sdf.format(date);
		return dateS;
	}
	
	public static String dateTimeToString (Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String dateS = sdf.format(date);
		return dateS;
	}

}
