package com.htec.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;


public class Game implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2481341099736077261L;

	
	private String leagueTitle;
	private Integer matchday;
	private String group;
	private String homeTeam ;
	private String awayTeam;
	private Date kickoffAt;
	private String score;
	public String getLeagueTitle() {
		return leagueTitle;
	}
	public void setLeagueTitle(String leagueTitle) {
		this.leagueTitle = leagueTitle;
	}
	public Integer getMatchday() {
		return matchday;
	}
	public void setMatchday(Integer matchday) {
		this.matchday = matchday;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}
	public String getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}
	public Date getKickoffAt() {
		return kickoffAt;
	}
	public void setKickoffAt(Date kickoffAt) {
		this.kickoffAt = kickoffAt;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	
	
	
	
}
