package com.htec.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Table implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4582569292472714118L;
	
	private String leagueTitle;
	private Integer matchDay;
	private String group;
	private ArrayList<ClubTable> standing;
	public String getLeagueTitle() {
		return leagueTitle;
	}
	public void setLeagueTitle(String leagueTitle) {
		this.leagueTitle = leagueTitle;
	}
	public Integer getMatchDay() {
		return matchDay;
	}
	public void setMatchDay(Integer matchDay) {
		this.matchDay = matchDay;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public ArrayList<ClubTable> getStanding() {
		return standing;
	}
	public void setStanding(ArrayList<ClubTable> standing) {
		this.standing = standing;
	}
	
	
	
	
}
