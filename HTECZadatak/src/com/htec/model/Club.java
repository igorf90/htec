package com.htec.model;

import java.io.Serializable;

public class Club implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5706785476314093813L;
	
	
	
	private String teamName;
	private Integer points;
	private Opponent opponent1;
	private Opponent opponent2;
	private Opponent opponent3;
	private Integer goals;
	private Integer goalsAgainst;
	private Integer goalsTemp;
	private Integer goalsAgainstTemp;
	private Integer rank;
	private Integer win;
	private Integer draw;
	private Integer lose;
	private String group;
	private Integer goalDifference;
	private ClubTable clubTable;
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public Opponent getOpponent1() {
		return opponent1;
	}
	public void setOpponent1(Opponent opponent1) {
		this.opponent1 = opponent1;
	}
	public Opponent getOpponent2() {
		return opponent2;
	}
	public void setOpponent2(Opponent opponent2) {
		this.opponent2 = opponent2;
	}
	public Opponent getOpponent3() {
		return opponent3;
	}
	public void setOpponent3(Opponent opponent3) {
		this.opponent3 = opponent3;
	}
	public Integer getGoals() {
		return goals;
	}
	public void setGoals(Integer goals) {
		this.goals = goals;
	}
	public Integer getGoalsAgainst() {
		return goalsAgainst;
	}
	public void setGoalsAgainst(Integer goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}
	public Integer getWin() {
		return win;
	}
	public void setWin(Integer win) {
		this.win = win;
	}
	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	public Integer getLose() {
		return lose;
	}
	public void setLose(Integer lose) {
		this.lose = lose;
	}
	public Integer getGoalsTemp() {
		return goalsTemp;
	}
	public void setGoalsTemp(Integer goalsTemp) {
		this.goalsTemp = goalsTemp;
	}
	public Integer getGoalsAgainstTemp() {
		return goalsAgainstTemp;
	}
	public void setGoalsAgainstTemp(Integer goalsAgainstTemp) {
		this.goalsAgainstTemp = goalsAgainstTemp;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public ClubTable getClubTable() {
		return clubTable;
	}
	public void setClubTable(ClubTable clubTable) {
		this.clubTable = clubTable;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Integer getGoalDifference() {
		return goalDifference;
	}
	public void setGoalDifference(Integer goalDifference) {
		this.goalDifference = goalDifference;
	}
	 

}
