package com.htec.model;

import java.io.Serializable;

public class ClubTable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4503104616529759104L;
	
	private Integer rank;
	private String team;
	private Integer playedGames;
	private Integer points;
	private Integer goals;
	private Integer goalsAgainst;
	private Integer goalDifference;
	private Integer win;
	private Integer lose;
	private Integer draw;
	private String group;
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public Integer getPlayedGames() {
		return playedGames;
	}
	public void setPlayedGames(Integer playedGames) {
		this.playedGames = playedGames;
	}
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public Integer getGoals() {
		return goals;
	}
	public void setGoals(Integer goals) {
		this.goals = goals;
	}
	public Integer getGoalsAgainst() {
		return goalsAgainst;
	}
	public void setGoalsAgainst(Integer goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}
	public Integer getGoalDifference() {
		return goalDifference;
	}
	public void setGoalDifference(Integer goalDifference) {
		this.goalDifference = goalDifference;
	}
	public Integer getWin() {
		return win;
	}
	public void setWin(Integer win) {
		this.win = win;
	}
	public Integer getLose() {
		return lose;
	}
	public void setLose(Integer lose) {
		this.lose = lose;
	}
	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	
	
	

}
