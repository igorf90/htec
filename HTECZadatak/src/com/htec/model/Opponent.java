package com.htec.model;

import java.io.Serializable;

public class Opponent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1604395807774458565L;
	
	private Integer goals;
	private Integer goalsAgainst;
	private String clubName;
	private Integer goalDifference;
	public Integer getGoals() {
		return goals;
	}
	public void setGoals(Integer goals) {
		this.goals = goals;
	}
	public Integer getGoalsAgainst() {
		return goalsAgainst;
	}
	public void setGoalsAgainst(Integer goalsAgainst) {
		this.goalsAgainst = goalsAgainst;
	}
	public String getClubName() {
		return clubName;
	}
	public void setClubName(String clubName) {
		this.clubName = clubName;
	}
	public Integer getGoalDifference() {
		return goalDifference;
	}
	public void setGoalDifference(Integer goalDifference) {
		this.goalDifference = goalDifference;
	}
	
	
	

}
