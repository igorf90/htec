package com.htec.bl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import com.htec.connection.DBConnector;
import com.htec.model.Club;
import com.htec.model.ClubTable;
import com.htec.model.Game;
import com.htec.model.Opponent;
import com.htec.model.Table;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

public class TableBL {
	
	public Table getTable (ArrayList<Game> gameRequest) {
		Club arsenal = new Club();
		Club PSG = new Club();
		Club basel = new Club();
		Club ludogorets = new Club();
		int arsenalIterator = 0;
		int PSGIterator = 0;
		int baselIterator = 0;
		int ludogoretsIterator = 0;
		for (int i=0;i<gameRequest.size();i++) {
			Game game = gameRequest.get(i);
			String homeTeam = game.getHomeTeam();
			String awayTeam = game.getAwayTeam();
			String score = game.getScore();
			String[] scoreA = score.split(":");
			String goalsHomeTeam = scoreA[0];
			String goalsAwayTeam = scoreA[1];
			if (homeTeam.equals("Arsenal") || awayTeam.equals("Arsenal")) {
				setClubValues(homeTeam, awayTeam, arsenal, game, goalsHomeTeam, goalsAwayTeam, "Arsenal", arsenalIterator);
				arsenalIterator++;
			} 
			if (homeTeam.equals("Basel") || awayTeam.equals("Basel")) {
				setClubValues(homeTeam, awayTeam, basel, game, goalsHomeTeam, goalsAwayTeam, "Basel", baselIterator);
				baselIterator++;			
			} 
			if (homeTeam.equals("PSG") || awayTeam.equals("PSG")) {
				setClubValues(homeTeam, awayTeam, PSG, game, goalsHomeTeam, goalsAwayTeam, "PSG", PSGIterator);
				PSGIterator++;
			}
			if (homeTeam.equals("Ludogorets") || awayTeam.equals("Ludogorets")){
				setClubValues(homeTeam, awayTeam, ludogorets, game, goalsHomeTeam, goalsAwayTeam, "Ludogorets", ludogoretsIterator);
				ludogoretsIterator++;
			}
			
		}
		Table table = new Table();
		
		table.setLeagueTitle("Champions league 2016/17");
		table.setMatchDay(6);
		table.setGroup("A");
		
		ArrayList<ClubTable> clubTables = new ArrayList<ClubTable>();
		ClubTable arsenalCT = returnClubTableValues(arsenal, "Arsenal");
		ClubTable ludogoretsCT = returnClubTableValues(ludogorets, "Ludogorets");
		ClubTable baselCT = returnClubTableValues(basel, "Basel");
		ClubTable PSGCT = returnClubTableValues(PSG, "PSG");
		arsenal.setClubTable(arsenalCT);
		arsenal.setTeamName("Arsenal");
		arsenal.setGoalDifference(arsenal.getGoals()-arsenal.getGoalsAgainst());
		ludogorets.setClubTable(ludogoretsCT);
		ludogorets.setTeamName("Ludogorets");
		ludogorets.setGoalDifference(ludogorets.getGoals()-ludogorets.getGoalsAgainst());
		basel.setClubTable(baselCT);
		basel.setTeamName("Basel");
		basel.setGoalDifference(basel.getGoals()-basel.getGoalsAgainst());
		PSG.setClubTable(PSGCT);
		PSG.setTeamName("PSG");
		PSG.setGoalDifference(PSG.getGoals()-PSG.getGoalsAgainst());
		clubTables.add(arsenalCT);
		clubTables.add(ludogoretsCT);
		clubTables.add(baselCT);
		clubTables.add(PSGCT);
		for (int i=0;i<clubTables.size();i++) {
			ClubTable clubTable = clubTables.get(i);
			saveClubTable(clubTable);
		}
		HashMap savedTables = getSavedTable("A");
		table = (Table) savedTables.get("tableA");
		
		return table;
	}

	
	private void setClubValues(String homeTeam, String awayTeam, Club club, Game game, String goalsHomeTeam, String goalsAwayTeam, String clubName, int iterator) {
		Opponent opponent = new Opponent();
		club.setGoalsTemp(null);
		club.setGoalsAgainstTemp(null);
		club.setGroup(game.getGroup());
		if (homeTeam.equals(clubName)) {
			if (club.getGoals() != null ) {
				club.setGoals(club.getGoals() + Integer.valueOf(goalsHomeTeam));
			} else {
				club.setGoals(Integer.valueOf(goalsHomeTeam));
			}
			if (club.getGoalsAgainst() != null) {
				club.setGoalsAgainst(club.getGoalsAgainst() + Integer.valueOf(goalsAwayTeam));
			} else {
				club.setGoalsAgainst(Integer.valueOf(goalsAwayTeam));
			}
			club.setGoalsTemp(Integer.valueOf(goalsHomeTeam));
			club.setGoalsAgainstTemp(Integer.valueOf(goalsAwayTeam));
			
			opponent.setClubName(game.getAwayTeam());
			opponent.setGoals(Integer.valueOf(goalsAwayTeam));
			opponent.setGoalsAgainst(Integer.valueOf(goalsHomeTeam));
			opponent.setClubName(awayTeam);
			opponent.setGoalDifference(Integer.valueOf(goalsAwayTeam) - Integer.valueOf(goalsHomeTeam));
		} else {
			if (club.getGoals() != null) {
				club.setGoals(Integer.valueOf(club.getGoals() + Integer.valueOf(goalsAwayTeam)));
			} else {
				club.setGoals(Integer.valueOf(Integer.valueOf(goalsAwayTeam).intValue()));
			}
			if (club.getGoalsAgainst() != null ) {
				club.setGoalsAgainst(club.getGoalsAgainst() + Integer.valueOf(goalsHomeTeam));
			} else {
				club.setGoalsAgainst(Integer.valueOf(goalsHomeTeam));
			}
			club.setGoalsTemp(Integer.valueOf(goalsAwayTeam));
			club.setGoalsAgainstTemp(Integer.valueOf(goalsHomeTeam));
			opponent.setClubName(homeTeam);
			opponent.setGoals(Integer.valueOf(goalsHomeTeam));
			opponent.setGoalsAgainst(Integer.valueOf(goalsAwayTeam));
			opponent.setGoalDifference(Integer.valueOf(goalsHomeTeam) - Integer.valueOf(goalsAwayTeam));
		}
		if (club.getGoalsTemp() == club.getGoalsAgainstTemp()) {
			if (club.getPoints() != null ) {
				club.setPoints(Integer.valueOf(club.getPoints() + 1));
			} else {
				club.setPoints(Integer.valueOf(1));
			}
			if (club.getDraw() != null) {
				club.setDraw(club.getDraw()+ 1);
			} else {
				club.setDraw(1);
			}
		} else if (club.getGoalsTemp() > club.getGoalsAgainstTemp()) {
			if (club.getPoints() != null) {
				club.setPoints(Integer.valueOf((club.getPoints() + 3)));
				
			} else {
				club.setPoints(Integer.valueOf((3)));
			}
			if (club.getWin() != null) {
				club.setWin(club.getWin()+ 1);
			} else {
				club.setWin(1);
			}
			
		} else {
			if (club.getLose() != null) {
				club.setLose(club.getLose()+ 1);
			} else {
				club.setLose(1);
			}
		}
		if (iterator == 0) {
			club.setOpponent1(opponent);
		} else if (iterator == 1) {
			club.setOpponent2(opponent);
		} else if (iterator == 2){
			club.setOpponent3(opponent);
		}
		if (iterator > 2) {
			if (club.getOpponent1().getClubName().equals(opponent.getClubName())) {
				club.getOpponent1().setGoals(opponent.getGoals() + club.getOpponent1().getGoals());
				club.getOpponent1().setGoalsAgainst(opponent.getGoalsAgainst() + club.getOpponent1().getGoalsAgainst());
				club.getOpponent1().setGoalDifference(opponent.getGoals() + club.getOpponent1().getGoals() - opponent.getGoalsAgainst() - club.getOpponent1().getGoalsAgainst());
//				club.setOpponent1(club.getOpponent1());
			} else if (club.getOpponent2().getClubName().equals(opponent.getClubName())) {
				club.getOpponent2().setGoals(opponent.getGoals() + club.getOpponent2().getGoals());
				club.getOpponent2().setGoalsAgainst(opponent.getGoalsAgainst() + club.getOpponent2().getGoalsAgainst());
				club.getOpponent2().setGoalDifference(opponent.getGoals() + club.getOpponent2().getGoals() - opponent.getGoalsAgainst() - club.getOpponent2().getGoalsAgainst());
//				club.setOpponent2(club.getOpponent2());
			} else if (club.getOpponent3().getClubName().equals(opponent.getClubName())) {
				club.getOpponent3().setGoals(opponent.getGoals() + club.getOpponent3().getGoals());
				club.getOpponent3().setGoalsAgainst(opponent.getGoalsAgainst() + club.getOpponent3().getGoalsAgainst());
				club.getOpponent3().setGoalDifference(opponent.getGoals() + club.getOpponent3().getGoals() - opponent.getGoalsAgainst() - club.getOpponent3().getGoalsAgainst() );
//				club.setOpponent3(club.getOpponent3());
			}
		}
			
	}
	
	private ClubTable returnClubTableValues (Club club, String clubName) {
		ClubTable clubTable = new ClubTable();
		
		clubTable.setDraw(club.getDraw() != null ? club.getDraw():0);
		clubTable.setGoalDifference(club.getGoals() - club.getGoalsAgainst());
		clubTable.setGoals(club.getGoals() != null ? club.getGoals():0);
		clubTable.setGoalsAgainst(club.getGoalsAgainst() != null ? club.getGoalsAgainst():0);
		clubTable.setLose(club.getLose() != null ? club.getLose():0);
		clubTable.setPlayedGames(6);
		clubTable.setPoints(club.getPoints() != null ? club.getPoints():0);
		clubTable.setTeam(clubName);
		clubTable.setWin(club.getWin() != null ? club.getWin():0);
		clubTable.setGroup("A");
		
		return clubTable;
	}
	
	
	//Ovaj metod bi trebao da sortira po medjusobnoj gol razlici ako imaju isti broj bodova i ako imaju istu gol razliku, ali sam njega radio na kraju i 
	//i nisam dovoljno promislio kako da ga iskoristim
	@SuppressWarnings("unchecked")
	private ArrayList fixStanding (ArrayList table) {
		ArrayList standing = new ArrayList();
		TreeMap<Integer, Club> treeMap = new TreeMap<>();
		Club tempClub = null;;
		for (int i=0;i<table.size();i++) {
			Club club = (Club) table.get(i);
			if (i == 0) {
				tempClub = club;
				club.setRank(1);
				treeMap.put(club.getRank(), club);
			} else {
				if (tempClub.getPoints() == club.getPoints() && tempClub.getGoalDifference() == club.getGoalDifference()) {
					if ((tempClub.getTeamName().equals(club.getOpponent1().getClubName()) && club.getOpponent1().getGoalDifference() >=1)
							|| (tempClub.getTeamName().equals(club.getOpponent2().getClubName()) && club.getOpponent2().getGoalDifference() >=1)
							|| (tempClub.getTeamName().equals(club.getOpponent3().getClubName()) && club.getOpponent3().getGoalDifference() >=1)) {
						club.setRank(tempClub.getRank());
						tempClub.setRank(tempClub.getRank()+1);
						treeMap.put(club.getRank(), club);
						treeMap.put(tempClub.getRank(), tempClub);
					}
				} else {
					tempClub = club;
				}
			}
		}
		standing.add(treeMap.get(1));
		standing.add(treeMap.get(2));
		standing.add(treeMap.get(3));
		standing.add(treeMap.get(4));
		
		return standing;
	}
	
	private void saveClubTable (ClubTable clubTable) {
		DBConnector connector = new DBConnector();
		Connection connection = (Connection) connector.connection();
		Statement stmt;
		try {
				stmt = (Statement) connection.createStatement();

				String sql = "INSERT INTO CLUBTABLE (TEAM, PLAYEDGAMES, POINTS, GOALS, GOALSAGAINST, GOALDIFFERENCE, WIN, LOSE, DRAW, TABLEGROUP) VALUES ('" + 
						  clubTable.getTeam() + "','" + clubTable.getPlayedGames() +"','"+
						clubTable.getPoints() +"','"+clubTable.getGoals() +"','"+clubTable.getGoalsAgainst() +"','"+clubTable.getGoalDifference() 
						+"','"+clubTable.getWin() +"','"+clubTable.getLose() +"','"+clubTable.getDraw()+"','"+clubTable.getGroup() +"')";
				stmt.executeUpdate(sql);
				
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public HashMap getSavedTable(String group) {
		Table tableA = null;
		Table tableB = null;
		Table tableC = null;
		Table tableD = null;
		DBConnector connector = new DBConnector();
		Connection connection = (Connection) connector.connection();
		HashMap tables = new HashMap();
		ArrayList standingA = new ArrayList();
		ArrayList standingB = new ArrayList();
		ArrayList standingC = new ArrayList();
		ArrayList standingD = new ArrayList();
		Statement stmt;
		try {
			
			stmt = (Statement) connection.createStatement();
			String wherePart = "";
			if (!group.equals("0")) {
				group = group.replaceAll("[-]", "','");
				wherePart = "WHERE TABLEGROUP IN ('" + group + "')";
			}
			String sql1 = " SELECT * FROM CLUBTABLE " + wherePart + " ORDER BY POINTS DESC, GOALS DESC";
					
			ResultSet rs=(ResultSet) stmt.executeQuery(sql1); 
			int rank = 1;
			while(rs.next())  {
				ClubTable clubTable = new ClubTable();
				clubTable.setDraw((int) rs.getLong("DRAW"));
				clubTable.setGoalDifference((int) rs.getLong("GOALDIFFERENCE"));
				clubTable.setGoals((int) rs.getLong("GOALS"));
				clubTable.setGoalsAgainst((int) rs.getLong("GOALSAGAINST"));
				clubTable.setLose((int) rs.getLong("LOSE"));
				clubTable.setPlayedGames((int) rs.getLong("PLAYEDGAMES"));
				clubTable.setPoints((int) rs.getLong("POINTS"));
				clubTable.setRank(rank);
				clubTable.setTeam(rs.getString("TEAM"));
				clubTable.setWin((int) rs.getLong("WIN"));
				clubTable.setGroup(rs.getString("TABLEGROUP"));
				if (rs.getString("TABLEGROUP").equals("A")) {
					standingA.add(clubTable);
				} else if (rs.getString("TABLEGROUP").equals("B")) {
					standingB.add(clubTable);
				} else if (rs.getString("TABLEGROUP").equals("C")) {
					standingC.add(clubTable);
				} else if (rs.getString("TABLEGROUP").equals("D")) {
					standingD.add(clubTable);
				}
				rank++;
				
			}
			if (!standingA.isEmpty()) {
				tableA = new Table();
				tableA.setGroup("A");
				tableA.setLeagueTitle("Champions league 2016/17");
				tableA.setMatchDay(6);
				tableA.setStanding(standingA);
				tables.put("tableA", tableA);
			} 
			if (!standingB.isEmpty()) {
				tableB = new Table();
				tableB.setGroup("B");
				tableB.setLeagueTitle("Champions league 2016/17");
				tableB.setMatchDay(6);
				tableB.setStanding(standingB);
				tables.put("tableB", tableB);
			} 
			if (!standingC.isEmpty()) {
				tableC = new Table();
				tableC.setGroup("C");
				tableC.setLeagueTitle("Champions league 2016/17");
				tableC.setMatchDay(6);
				tableC.setStanding(standingC);
				tables.put("tableC", tableC);
			} 
			if (!standingD.isEmpty()) {
				tableD = new Table();
				tableD.setGroup("D");
				tableD.setLeagueTitle("Champions league 2016/17");
				tableD.setMatchDay(6);
				tableD.setStanding(standingD);
				tables.put("tableD", tableD);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tables;
	}
	
	public ArrayList returnSavedTable (String group) {
		ArrayList tables = new ArrayList();
		HashMap savedTables = getSavedTable(group);
		Table tableA = (Table) savedTables.get("tableA");
		Table tableB = (Table) savedTables.get("tableB");
		Table tableC = (Table) savedTables.get("tableC");
		Table tableD = (Table) savedTables.get("tableD");
		if (tableA != null) {
			tables.add(tableA);
		}
		if (tableB != null) {
			tables.add(tableB);
		}
		if (tableC != null) {
			tables.add(tableC);
		}
		if (tableD != null) {
			tables.add(tableD);
		}
		return tables;
	}

}
