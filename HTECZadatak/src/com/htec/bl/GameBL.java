package com.htec.bl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.htec.connection.DBConnector;
import com.htec.model.ClubTable;
import com.htec.model.Game;
import com.htec.request.ChangeResultRequest;
import com.htec.request.GameRequest;
import com.htec.util.DateUtil;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

public class GameBL {
	
	public void saveGame (ArrayList games) {
		DBConnector connector = new DBConnector();
		Connection connection = (Connection) connector.connection();
		Statement stmt;
		try {
				stmt = (Statement) connection.createStatement();
				if (games != null && !games.isEmpty()) {
					for (int i=0;i<games.size();i++) {
						Game game = (Game) games.get(i);
						String sql = "INSERT INTO GAME (LEAGUETITLE, MATCHDAY, HOMETEAM, AWAYTEAM, TABLEGROUP, KICKOFFAT, SCORE) VALUES ('" + 
								game.getLeagueTitle() + "','" + game.getMatchday() + "','" + game.getHomeTeam() +"','"+
								game.getAwayTeam() + "','"+ game.getGroup() +"','"+DateUtil.dateToString(game.getKickoffAt()) +"','"+game.getScore() +"')";
						stmt.executeUpdate(sql);
					}
					
				}
				
				
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public ArrayList returnMatchResults(GameRequest gameRequest) {
		ArrayList<Game> matchResults = new ArrayList<Game>();
		boolean firstCondition = false;
		String wherePart = "  ";
		if (gameRequest.getFromDate() != null) {
			wherePart += " WHERE KICKOFFAT >= '" + DateUtil.dateTimeToString(gameRequest.getFromDate()) + "' "; 
			firstCondition = true;
 		}
		if (gameRequest.getToDate() != null) {
			if (firstCondition) {
				wherePart += " AND KICKOFFAT <= '"  + DateUtil.dateTimeToString(gameRequest.getToDate()) + "' ";
			} else {
				wherePart += " WHERE KICKOFFAT <= '"  + DateUtil.dateTimeToString(gameRequest.getToDate()) + "' ";
			}
		}
		if (gameRequest.getTeam() != null) {
			if (firstCondition) {
				wherePart += " AND HOMETEAM = '"  + gameRequest.getTeam() + "' OR AWAYTEAM = '"  + gameRequest.getTeam() + "' ";
			} else {
				wherePart += " WHERE HOMETEAM = '"  + gameRequest.getTeam() + "' OR AWAYTEAM = '"  + gameRequest.getTeam() + "' ";
			}	
		}
		if (gameRequest.getGroup() != null) {
			if (firstCondition) {
				wherePart += " AND TABLEGROUP = '"   + gameRequest.getGroup() + "' ";
			} else {
				wherePart += " WHERE TABLEGROUP <= '"  + gameRequest.getGroup() + "' ";
			}
			
		}
		DBConnector connector = new DBConnector();
		Connection connection = (Connection) connector.connection();
		Statement stmt;
		try {
			
			stmt = (Statement) connection.createStatement();
			
			String sql1 = " SELECT * FROM GAME " + wherePart;
					
			ResultSet rs=(ResultSet) stmt.executeQuery(sql1); 
			while(rs.next())  {
				Game game = new Game();
				game.setAwayTeam(rs.getString("AWAYTEAM"));  
				game.setHomeTeam(rs.getString("HOMETEAM"));
				game.setKickoffAt(rs.getDate("KICKOFFAT"));
				game.setLeagueTitle(rs.getString("LEAGUETITLE"));
				game.setMatchday(((int) rs.getLong("MATCHDAY")));
				game.setScore(rs.getString("SCORE"));
				matchResults.add(game);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return matchResults;
	}
	
	public void updateResult (ArrayList<ChangeResultRequest> changeResultRequests) {
		
		DBConnector connector = new DBConnector();
		Connection connection = (Connection) connector.connection();
		Statement stmt;
		try {
				stmt = (Statement) connection.createStatement();
				for (int i=0;i<changeResultRequests.size();i++) {
					ChangeResultRequest changeResultRequest = changeResultRequests.get(i);
					String sql = " UPDATE GAME SET SCORE = '" + changeResultRequest.getResult() + "' WHERE HOMETEAM = '" + changeResultRequest.getHomeTeam() + "' AND AWAYTEAM = '" + changeResultRequest.getAwayTeam() + "'";
					stmt.executeUpdate(sql);
				}
				
				
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		
	}

}
