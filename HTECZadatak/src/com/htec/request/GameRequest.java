package com.htec.request;

import java.io.Serializable;
import java.util.Date;

public class GameRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7256057404828820149L;
	
	private Date fromDate;
	private Date toDate;
	private String group;
	private String team;
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	

}
