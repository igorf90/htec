package com.htec.request;

import java.io.Serializable;

public class ChangeResultRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5664315371072846049L;
	
	private String homeTeam;
	private String awayTeam;
	private String result;
	public String getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}
	public String getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}

}
